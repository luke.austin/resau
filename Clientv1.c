/* Client v4 */
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>

int isin(char c, char *s)
{
	int i;
	for(i = 0; i < strlen(s);i++)
	{
		if(c == s[i])
		{
			return 1;
		}
	}
	return 0;
}

void answerQuestion(char *buffer,int D_Socket_Client)
{
	int cont = 1;
	int n;
  	while(cont)
  	{
		bzero((void *)buffer,1024);
		n=recv(D_Socket_Client, buffer, 1024, 0);
  		printf("Data: %s",buffer);   
		
		if(isin('*',buffer))
		{	
			printf("%s","End of file \n");
			cont = 0;
		}
		else if(n < 1)
		{
			printf("%s","Transmision error \n");
			cont = 0;
		}
		else
		{
			bzero((void *)buffer,1024);
			scanf("%s",buffer);
			send(D_Socket_Client,buffer,13,0);
		}
	}
}

int main(){
  int D_Socket_Client;
  char buffer[1024];
  struct sockaddr_in Struct_Addr_Serv;
  socklen_t addr_size;

  D_Socket_Client = socket(PF_INET, SOCK_STREAM, 0);
  Struct_Addr_Serv.sin_family = AF_INET;
  Struct_Addr_Serv.sin_port = htons(7891);
  Struct_Addr_Serv.sin_addr.s_addr = inet_addr("127.0.0.1");
  memset(&(Struct_Addr_Serv.sin_zero), '\0', sizeof Struct_Addr_Serv.sin_zero);  
  addr_size = sizeof Struct_Addr_Serv;

  connect(D_Socket_Client, (struct sockaddr *) &Struct_Addr_Serv, addr_size);
  
  answerQuestion(buffer,D_Socket_Client);
	return 0;
}
