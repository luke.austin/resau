/* Server v4 */
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <strings.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

void answerQuestion(char *buffer,int D_Socket_Com)
{
	int cont = 1;
	int next = 0;
	int score = 0;
	FILE * fp;
	char * line = NULL;	
	char pseudo[1024];	
	char sscore[10];
	size_t len = 0;
	fp = fopen("answers.txt","r");
	if(fp == NULL)
	{
		printf("file answers.txt missing\n");
		exit(1);
	}
	bzero((void *)buffer,1024);
	strcpy(buffer,"Veuilliez entrer un pseudonym:\n");
	send(D_Socket_Com,buffer,strlen(buffer),0); /* >>> Send prompt. */
	bzero((void *)buffer,1024);
	recv(D_Socket_Com,pseudo,1025,0); /* <<< Get pseudonym */
	printf("Received pseudonym %s \n",pseudo);
	bzero((void *)buffer,1024);
	bzero(line,len);	
	getline(&line, &len,fp); /* <<< Get 1rst question */

	do
	{
		strcat(buffer,line);
		printf("sending %s",buffer);	
		send(D_Socket_Com,buffer,strlen(buffer),0); /* >>> Send Result+Question */	
		bzero((void *)buffer,1024);
		recv(D_Socket_Com,buffer,1025,0); /* <<< Get response */
		printf("Received %s \n",buffer);
		bzero(line,len);	
		getline(&line, &len,fp); /* <<< Get correct answer */	
		if(strncmp("end",buffer,1)==0)
		{
			bzero((void *)buffer,1024);
			strcpy(buffer,"Au revoir.\n");	
			cont = 0;
		}
		else if(strncmp(line,buffer,1)==0)
		{
			score++;
			bzero((void *)buffer,1024);
			strcpy(buffer,"Correct.\n");
		}
		else
		{
			bzero((void *)buffer,1024);
			strcpy(buffer,"Faux.\n");
		}
		bzero(line,len);	
		next = getline(&line, &len,fp); /* <<< Get question */
		if(next == -1)
		{
			printf("Reached end of file...");
			strcat(buffer,"end of file.\n");
			cont = 0;	
		}	
	}while(cont);

	fclose(fp);	
	fp = fopen("scoreboard.txt","a+");
	strcat(pseudo,"\t\t");
	sprintf(sscore,"%d\n",score); /* format score number */
	strcat(pseudo,sscore); /* add score to pseudonym*/
	fwrite((void *)pseudo,sizeof(char),strlen(pseudo),fp); /* >>> write score to file */
	strcat(buffer,"*********Palmarès********\n");
	rewind(fp);
	do{
		next = getline(&line,&len,fp); /* <<< Read from file */
		if(next != -1){strcat(buffer,line);}
	}while(next != -1);
	strcat(buffer,"*************************\n");
	send(D_Socket_Com,buffer,strlen(buffer),0); /* >>> Send Result. */	
	fclose(fp);
}

int main(){
	int D_Socket_Ecoute, D_Socket_Com;
	int i;
	char buffer[1024];
	bzero((void *)buffer,1024);
	struct sockaddr_in Struct_Addr_Serv;
	struct sockaddr_in Struct_Addr_Rec_Cli;
	socklen_t addr_size;

	D_Socket_Ecoute = socket(PF_INET, SOCK_STREAM, 0);
	Struct_Addr_Serv.sin_family = AF_INET;
	Struct_Addr_Serv.sin_port = htons(7891); /* host to network short */
	Struct_Addr_Serv.sin_addr.s_addr = inet_addr("127.0.0.1");
	memset(&(Struct_Addr_Serv.sin_zero), '\0', sizeof(Struct_Addr_Serv.sin_zero));  

	bind(D_Socket_Ecoute, (struct sockaddr *) &Struct_Addr_Serv, sizeof(struct sockaddr));


	if(listen(D_Socket_Ecoute,5)==0) printf("Listening\n");
	else printf("Error\n");
	addr_size = sizeof Struct_Addr_Rec_Cli;

	for(i = 0; i<2 ;i++)
	{
		D_Socket_Com = accept(D_Socket_Ecoute, (struct sockaddr *) &Struct_Addr_Rec_Cli, &addr_size);
		printf("connection received as %d with socket number %d\n",getpid(),D_Socket_Com);
		if(fork() == 0)
		{
			answerQuestion(buffer,D_Socket_Com);
			exit(0);
		}
	}
	int wpid;
	while((wpid = wait(NULL)) > 0)
	       sleep(2);	
	
	close(D_Socket_Ecoute);
	return 0;
}
